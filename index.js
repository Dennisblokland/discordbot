const Discord = require("discord.js");
const bot = new Discord.Client();

const config = require("./config.json");

bot.on("guildMemberAdd", member => {
    let guild = member.guild;
    guild.defaultChannel.sendMessage(`U wot m8, ${member.user}!`);
});

bot.on("message", (message) => {
    if(message.author.bot) return;

    let command = message.content.split(" ")[0];
    command = command.slice(config.prefix.length);

    let args = message.content.split(" ").slice(1);

    //tekst output

    if(command == "say"){
        message.channel.sendMessage(args.join(" "));
    }

    if(message.content == "ayy"){
        message.channel.sendMessage("lmao");
    }

    // functional output

    if(command == "add"){
        let numArray = args.map(n=> parseInt(n));
        let total = numArray.reduce( (p, c) => p+c);

        message.channel.sendMessage(total);
    }

    if (command == "kick"){
      let modRole = message.guild.roles.find("name", "temp. Admin");
      if(!message.member.roles.has(modRole.id)){
           return message.reply("oi cunt you dont have permission!");
      }
      if(message.mentions.users.size == 0){
        return message.reply("Geef wel een naam mee om te kicken.");
      }
      let kickMember = message.guild.member(message.mentions.users.first());
      if (!kickMember){
        return message.reply("User doesn't exist");
      }
      if(!message.guild.member(bot.user).hasPermission("KICK_MEMBERS")){
        return message.reply("I don't have the permissions (KICK_MEMBERS) to do this");
      }
      kickMember.kick().then(member => {
        message.reply(`${member.user.username} Got kicked out!`);
      });
    }

    if (command == "ban"){
      let modRole = message.guild.roles.find("name", "temp. Admin");
      if(!message.member.roles.has(modRole.id)){
          return message.reply("oi cunt you dont have permission!");
      }
      if(message.mentions.users.size == 0){
        return message.reply("No name given.");
      }
      let banMember = message.guild.member(message.mentions.users.first());
      if (!banMember){
        return message.reply("User doesn't exist");
      }
      if(!message.guild.member(bot.user).hasPermission("BAN_MEMBERS")){
        return message.reply("I don't have the permissions (BAN_MEMBERS) to do this");
      }
      banMember.ban().then(member => {
        message.reply(`${member.user.username} Got sucked into a black hole!!`);
      });
    }

    // visual output

    if(command == "stfu"){
      message.channel.sendMessage("https://www.youtube.com/watch?v=OLpeX4RRo28");
    }
    if(command == "hentai"){
      message.channel.sendMessage("https://www.youtube.com/watch?v=HeFhAK-MDs4");
    }

    if(command == "yeet"){
      message.channel.sendMessage("http://imgur.com/gallery/bMajELQ");
    }
});

bot.login(config.token);
